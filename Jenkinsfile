pipeline {
    agent any
    stages {
       stage('build') {
          steps {
             echo 'Notify GitLab'
             updateGitlabCommitStatus name: 'build', state: 'pending'
             script {
               try {
                 echo 'build step goes here'
                 sh 'pip3 install -r requirements.txt'
                 sh 'python3 manage.py makemigrations'
                 sh 'python3 manage.py migrate'
                 updateGitlabCommitStatus name: 'build', state: 'success'
               } catch (Exception e) {
                 echo 'Exception occurred: ' + e.toString()
                 updateGitlabCommitStatus name: 'build', state: 'failed'
                 sh 'exit 1'
               }
             }
          }
       }
       stage(test) {
           steps {
               echo 'Notify GitLab'
               updateGitlabCommitStatus name: 'test', state: 'pending'
               script {
                 try {
                   echo 'test step goes here'
                   sh 'python3 manage.py test'
                   updateGitlabCommitStatus name: 'test', state: 'success'
                 } catch (Exception e) {
                   echo 'Exception occurred: ' + e.toString()
                   updateGitlabCommitStatus name: 'test', state: 'failed'
                   sh 'exit 1'
                 }
               }
           }
       }
       stage(linter) {
           steps {
               echo 'Notify GitLab'
               updateGitlabCommitStatus name: 'linter', state: 'pending'
               script {
                 try {
                   echo 'checks step goes here'
                   sh 'pycodestyle --max-line 120 .'
                   updateGitlabCommitStatus name: 'linter', state: 'success'
                 } catch (Exception e) {
                   echo 'Exception occurred: ' + e.toString()
                   updateGitlabCommitStatus name: 'linter', state: 'failed'
                   sh 'exit 1'
                 }
               }
           }
       }
       stage(security) {
           steps {
               echo 'Notify GitLab'
               updateGitlabCommitStatus name: 'security', state: 'pending'
               script {
                 try {
                   echo 'security checks step goes here'
                   sh 'safety check'
                   sh 'python3 manage.py check --deploy'
                   updateGitlabCommitStatus name: 'security', state: 'success'
                 } catch (Exception e) {
                   echo 'Exception occurred: ' + e.toString()
                   updateGitlabCommitStatus name: 'security', state: 'failed'
                   sh 'exit 1'
                 }
               }
           }
       }
       stage(seleniumsiderunner) {
           steps {
               echo 'Notify GitLab'
               updateGitlabCommitStatus name: 'seleniumsiderunner', state: 'pending'
               script {
                 try {
                   echo 'seleniumsiderunner step goes here'
                   wrap([
                     $class: 'Xvfb', additionalOptions: '', assignedLabels: '',
                             autoDisplayName: true, debug: true, displayNameOffset: 0,
                             installationName: 'xvfb', parallelBuild: true, screen: '1024x758x24', timeout: 30]
                   ) {
                     sh 'selenium-side-runner selenium/*.side -c "goog:chromeOptions.args=[--headless,--nogpu] browserName=chrome" --output-directory=results --output-format=junit'
                   }
                   updateGitlabCommitStatus name: 'seleniumsiderunner', state: 'success'
                 } catch (Exception e) {
                   echo 'Exception occurred: ' + e.toString()
                   updateGitlabCommitStatus name: 'seleniumsiderunner', state: 'failed'
                   sh 'exit 1'
                 }
               }
           }
       }
       stage(mavenrtest) {
           steps {
               echo 'Notify GitLab'
               updateGitlabCommitStatus name: 'mavenrtest', state: 'pending'
               script {
                 try {
                   echo 'mavenrtest step goes here'
                   wrap([
                     $class: 'Xvfb', additionalOptions: '', assignedLabels: '',
                             autoDisplayName: true, debug: true, displayNameOffset: 0,
                             installationName: 'xvfb', parallelBuild: true, screen: '1024x758x24', timeout: 30]
                   ) {
			withMaven (maven: 'maventheroot')
			{
			      sh "mvn clean test"

			}
                   }
                   updateGitlabCommitStatus name: 'mavenrtest', state: 'success'
                 } catch (Exception e) {
                   echo 'Exception occurred: ' + e.toString()
                   updateGitlabCommitStatus name: 'mavenrtest', state: 'failed'
                   sh 'exit 1'
                 }
               }
           }
       }
      stage(deploy) {
           environment {
               ACTUAL_BRANCH = "${GIT_BRANCH.split("/")[1]}/${GIT_BRANCH.split("/")[2]}"
           }
           steps {
               echo 'Notify GitLab'
               updateGitlabCommitStatus name: 'deploy', state: 'pending'
               script {
                 try {
                   echo 'deploy step goes here'
                   sshagent(['7fc07dcc-d226-4051-bd86-2101dba2b5df']) {
                      sh '''
                         ssh-keyscan 135.181.192.255 >> ~/.ssh/known_hosts &&
                         ssh -A root@135.181.192.255 "cd /var/django/djangotest && git checkout ${ACTUAL_BRANCH} && git pull"  &&
                         #ssh -A root@135.181.192.255 "cd /var/django/djangotest && source venv/bin/activate && pip3 install -r requirements.txt"  &&
                         ssh -A root@135.181.192.255 "cd /var/django/djangotest && source venv/bin/activate && python3 manage.py makemigrations && python3 manage.py migrate"  &&
                         ssh -A root@135.181.192.255 "service uwsgi reload"
                      '''
                       }
                   updateGitlabCommitStatus name: 'deploy', state: 'success'
                 } catch (Exception e) {
                   echo 'Exception occurred: ' + e.toString()
                   updateGitlabCommitStatus name: 'deploy', state: 'failed'
                   sh 'exit 1'
                 }
               }
           }
       }
    }
    post {
        always {
            junit 'results/*.xml'
        }
    }
 }
